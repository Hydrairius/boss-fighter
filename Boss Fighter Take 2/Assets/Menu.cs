﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    [SerializeField]
    private GameObject MainMenuPanel;
    [SerializeField]
    private GameObject CustomMatchMakingPanel;
    [SerializeField]
    private GameObject WinPanel;
    [SerializeField]
    private GameObject LosePanel;
    [SerializeField]
    private Dropdown DifficultyDropdown;
   public int Difficulty;

    public TextMeshProUGUI  HeroBossKillsTxt, ChampionBossKillsTxt, AdventurerBossTimeTxt, HeroBossTimeTxt, ChampionBossTimeTxt;

    public TextMeshProUGUI AdventurerBossKillsTxt;




    public int AdventurerBossKills;
    public int HeroBossKills;
    public int ChampionBossKills;

    public float AdventurerBestTime;
    public float HeroBestTime;
    public float ChampionBestTime;

    public int totalBossKills;

    private float win;

	// Use this for initialization
	void Start () {

       win = PlayerPrefs.GetFloat("Win");
        AdventurerBossKills = PlayerPrefs.GetInt("AdventurerKills");
        AdventurerBossKillsTxt.text = "Kills: " + AdventurerBossKills;

        HeroBossKills = PlayerPrefs.GetInt("HeroKills");
        HeroBossKillsTxt.text = "Kills: " + HeroBossKills;

        ChampionBossKills = PlayerPrefs.GetInt("ChampionKills");
        ChampionBossKillsTxt.text = "Kills: " + ChampionBossKills;


        Difficulty = PlayerPrefs.GetInt("Difficulty");

        AdventurerBestTime = PlayerPrefs.GetFloat("AdventurerTime");
        HeroBestTime = PlayerPrefs.GetFloat("HeroTime");
        ChampionBestTime = PlayerPrefs.GetFloat("ChampionTime");


       AdventurerBossTimeTxt.text = "Best Time: " + (Mathf.Round(AdventurerBestTime * 100f) / 100f) + " secs";
       HeroBossTimeTxt.text = "Best Time: " + (Mathf.Round(HeroBestTime * 100f) / 100f) + " secs";
       ChampionBossTimeTxt.text = "Best Time: " + (Mathf.Round(ChampionBestTime * 100f) / 100f) + " secs";

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;


        if(win == 1 )
        {
            WinPanel.SetActive(true);
            MainMenuPanel.SetActive(false);
            if (Difficulty == 0)
            {
                AdventurerBossKills = AdventurerBossKills+ 1;
                PlayerPrefs.SetInt("AdventurerKills", AdventurerBossKills);
                AdventurerBossKillsTxt.text = "Kills: " + AdventurerBossKills;
            } else if(Difficulty == 1){
                HeroBossKills = HeroBossKills + 1;
                PlayerPrefs.SetInt("HeroKills", HeroBossKills);
                HeroBossKillsTxt.text = "Kills: " + HeroBossKills;

            }
            else if (Difficulty == 2)
            {
                ChampionBossKills = ChampionBossKills + 1;
                PlayerPrefs.SetInt("ChampionKills", ChampionBossKills);
                ChampionBossKillsTxt.text = "Kills: " + ChampionBossKills;
            }
            PlayerPrefs.SetFloat("Win", 0);
            


        } else
        {
            if(win == 2)
            {
                LosePanel.SetActive(true);
                MainMenuPanel.SetActive(false);
                PlayerPrefs.SetFloat("Win", 0);

            }
            else
            {
                PlayerPrefs.SetFloat("Win", 0);
            }
        }

        Difficulty = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

    }

    public void DifficultyChangedValue()
    {
        Difficulty = DifficultyDropdown.value;
        PlayerPrefs.SetInt("Difficulty", DifficultyDropdown.value);
    }


    public void StartGame()
    {

       
        MainMenuPanel.SetActive(false);
        CustomMatchMakingPanel.SetActive(true);

    }

    public void Exit()
    {
        Application.Quit();
    }
}
