﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class GameSetupController : MonoBehaviour {
    public Vector3 spawn;

	// Use this for initialization
	void Start () {
        CreatePlayer();
	}

    private void CreatePlayer()
    {
        Debug.Log("Creating Player");
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs","PhotonPlayer"),spawn,Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
