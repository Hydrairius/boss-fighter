﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EatMe : MonoBehaviourPunCallbacks, IPunObservable {
    float timeToEat;
    public int myType; //0 = flower(hp boost)
    public float hpIGive, timeToEatOG, myCD;
    PlayerController playerInMe;
    GameObject myParticles;
    public AudioSource EatSound;

    

	// Use this for initialization
	void Start () {
        timeToEat = timeToEatOG;
        myParticles = GetComponentInChildren<ParticleSystem>().gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && playerInMe==null)
        {
            playerInMe = other.GetComponent<PlayerController>();
            UIManager.UIM.Comsume.gameObject.SetActive(true);


        }

        if (Input.GetButton("Interact"))
        {
            if (myParticles.activeSelf)
            {
                myParticles.SetActive(false);
            }
            timeToEat -= Time.deltaTime;
            if (timeToEat <= 0)
            {
                UIManager.UIM.Comsume.gameObject.SetActive(false);

                photonView.RPC("CallApplyEffect", RpcTarget.All);
                EatSound.Play();
                timeToEat = timeToEatOG;
                photonView.RPC("CallFade", RpcTarget.All);
            }
        }

        //if you let go of the button while eating and it's not done yet
        if (Input.GetButtonUp("Interact"))
        {
            if (!myParticles.activeSelf)
            {
                myParticles.SetActive(true);
            }
        }
        //else
        //{
        //    UIManager.UIM.Comsume.gameObject.SetActive(true);
        //}
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && playerInMe ==other.GetComponent<PlayerController>())
        {
            //get rid of the player that was once in me.
            playerInMe = null;
            myParticles.SetActive(true);
            UIManager.UIM.Comsume.gameObject.SetActive(false);
        }

    }

    /// <summary>
    /// 0=hpBoost
    /// </summary>
    /// <param name="whatType"></param>
    /// 

    [PunRPC]
    public void CallApplyEffect()
    {
        ApplyEffect(myType);
    }


    public void ApplyEffect(int whatType)
    {
        switch (whatType)
        {
            case 0:
                playerInMe.health += hpIGive;

                break;

            case 1:

                break;
        }
    }
    [PunRPC]
    public void CallFade()
    {
        StartCoroutine(FadeAway(myCD));
    }
    public IEnumerator FadeAway(float respawnTime)
    {
        //turn off trigger:
        GetComponent<SphereCollider>().enabled = false;
        //fade the renderer
        myParticles.SetActive(false);
        GetComponent<MeshRenderer>().enabled = false;
        yield return new WaitForSeconds(respawnTime);
        GetComponent<SphereCollider>().enabled = true;
        GetComponent<MeshRenderer>().enabled = true;
        myParticles.SetActive(true);

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

        if(stream.IsWriting)
        {
            stream.SendNext(timeToEat);
            stream.SendNext(myType);
            stream.SendNext(timeToEatOG);
            stream.SendNext(hpIGive);
            stream.SendNext(myCD);

        } else
        {
            timeToEat = (float)stream.ReceiveNext();
            myType = (int)stream.ReceiveNext();
            timeToEatOG = (float)stream.ReceiveNext();
            hpIGive = (float)stream.ReceiveNext();
            myCD = (float)stream.ReceiveNext();

        }
    }

    }
