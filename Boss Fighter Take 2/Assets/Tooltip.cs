﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {



    public Text tTitle, tDescription;


    public void showToolTip(string Title)
    {

        gameObject.SetActive(true);

        tTitle.text = Title;


    }

    public void tooltipDescription(string Description)
    {

        

        tDescription.text = Description;


    }


    public void hideToolTip()
    {
        gameObject.SetActive(false);
    }
}
