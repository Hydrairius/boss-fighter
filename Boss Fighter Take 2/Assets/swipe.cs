﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class swipe : MonoBehaviourPunCallbacks {

    public BossController boss;
    

	// Use this for initialization
	void Start () {

        
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnTriggerStay(Collider other)
    {

        if (!boss.playersInRange.Contains(other.gameObject))
        {
            boss.playersInRange.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (boss.playersInRange.Contains(other.gameObject))
        {
            boss.playersInRange.Remove(other.gameObject);
        }
    }
}
