﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TTHelper : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler{

    public string title, desc;
    public GameObject ttPrefab, ttRef;
    public Vector2 offset;


	public void OnPointerEnter(PointerEventData eventData)
    {
        ttRef = Instantiate(ttPrefab, transform, false);
        ttRef.transform.localPosition = offset;
        ttRef.GetComponent<Tooltip>().tTitle.text = title;
        ttRef.GetComponent<Tooltip>().tDescription.text = desc;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        if (ttRef != null)
        {
            Destroy(ttRef);
        }
    }
}
