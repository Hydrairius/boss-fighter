﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSlash : MonoBehaviour {

    
    public PlayerController player;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(gameObject.GetComponent<MeshCollider>().enabled == false)
        {
            player.boss = null;
        }
        if(player.slashed == false)
        {
            player.boss = null;
        }
	}

    private void OnTriggerStay(Collider other)
    {
        
        
        if(other.gameObject.tag != null)
        {
            
            if(other.gameObject.tag =="Boss")
            {
                
                if (other.gameObject.GetComponent<BossController>() != null)
                {
                    player.boss = other.gameObject.GetComponent<BossController>();
                    Debug.Log("Retrieving data from boss!");
                }

            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != null)
        {
            if (other.gameObject.tag == "Boss")
            {
               
                player.boss = null;

            }
        }
    }

    
}
