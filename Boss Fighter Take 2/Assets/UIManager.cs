﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class UIManager : MonoBehaviourPunCallbacks, IPunObservable {

    public static UIManager UIM;

    public Slider healthbar;

    public Slider playerHealthBar;

    public Slider playerEnergyBar;

    public TextMeshProUGUI TimerText;

    public BossController boss;

    public TextMeshProUGUI Comsume;

    public float bossHealth;

    // Use this for initialization
    void Start() {
        UIM = this;




    }

    // Update is called once per frame
    void Update() {
        healthbar.value = CalculateHealth();
        bossHealth = boss.Health;

    }

    float CalculateHealth()
    {
        return (boss.Health / boss.maxHealth);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {


        if (stream.IsWriting)
        {
            stream.SendNext(bossHealth);
            if(boss != null)
            {
                stream.SendNext(boss.Health);
            }
       
        } else
        {
            bossHealth = (float)stream.ReceiveNext();
            if(boss != null)
            {
                boss.Health = (float)stream.ReceiveNext();
            }
        }
    }




    }
