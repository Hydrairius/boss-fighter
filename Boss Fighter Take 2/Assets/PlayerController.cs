﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.AI;

public class PlayerController : MonoBehaviourPunCallbacks, IPunObservable
{

    public Rigidbody rb;
    public float speed;

    public float health;
    public float maxHealth;

    public float energy;
    public float maxEnergy;

    public float verticalVelocity;
    public float gravity = 14.0f;
    public float jumpforce = 10.0f;
    public bool gainingEnergy = false;

    public AudioSource Swing;
    public AudioSource Hurt;

    public CapsuleCollider SlashCollider;
    public MeshRenderer SlashRenderer;


    public bool slashed = false;

    public bool isGrounded = true;
    public int SlashCounter = 0;

    public bool isDead = false;

    public bool win = false;

    public bool nearDeadPlayer = false;

    public PlayerController deadPlayer;

    public PhotonView photonView;

    public int difficulty;

    public BossController boss;

    Animator anim;



    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        photonView = GetComponent<PhotonView>();
        anim = GetComponent<Animator>();

        photonView.ObservedComponents.Add(GetComponent<PlayerController>());

        

        health = maxHealth;

        SlashCollider.enabled = false;
        SlashRenderer.enabled = false;

        difficulty = PlayerPrefs.GetInt("Difficulty");





    }


    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            if (!isDead)
            {
                PlayerMovement();

                PlayerJump();

                UIManager.UIM.playerHealthBar.value = CalculateHealth();
                UIManager.UIM.playerEnergyBar.value = CalculateEnergy();
                CheckHP();

                if (gainingEnergy == false)
                {

                    StartCoroutine(Energy(5, 1));
                }

                PlayerAttack();
            }
            CheckHP();


        }


    }







    void PlayerMovement()
    {
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 playerMovemment = new Vector3(hor, 0f, ver) * speed * Time.deltaTime;
        transform.Translate(playerMovemment, Space.Self);
        
        
        if((hor >= 0.05) || (hor <= -0.05))
        {
            anim.SetBool("IsWalking", true);
        } else
        {
            if ((ver >= 0.05) || (ver <= -0.05))
            {
                anim.SetBool("IsWalking", true);
            }
            else
            {
                anim.SetBool("IsWalking", false);
            }
        }
    }

    void PlayerJump()
    {
        if (isGrounded)
        {
            verticalVelocity = -gravity * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                verticalVelocity = jumpforce;
            }
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }

        Vector3 moveVector = new Vector3(0, verticalVelocity, 0);
        rb.AddForce(moveVector * Time.deltaTime);
    }
    void CheckHP()
    {
      
        anim.SetFloat("mHealth", health);

        if (health <= 0)
        {
            isDead = true;

            


        }
        if(isDead == true)
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }

    }

    float CalculateHealth()
    {
        return (health / maxHealth);
       
    }

    float CalculateEnergy()
    {
        return (energy / maxEnergy);
    }


    
    void PlayerAttack()
    {
        if (Input.GetMouseButton(0))
        {
            if (!slashed)
            {
                Debug.Log("Slash");
                Swing.Play();
                StartCoroutine(Slash(25, 0.7f));
            }
        }
    }
    void RevivePlayer()
    {
        if (nearDeadPlayer)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                deadPlayer.StartCoroutine(Revive(1));
                nearDeadPlayer = false;
                deadPlayer = null;
            }
        }
    }



    IEnumerator Energy(float value, float delay)
    {
      
        gainingEnergy = true;
        yield return new WaitForSeconds(delay);

        if (energy < maxEnergy)
        {
            energy += value;
           
        }
        else if (energy == maxEnergy)
        {
            energy = maxEnergy;
        }
        gainingEnergy = false;

    }

    IEnumerator Revive(float delay)
    {
        yield return new WaitForSeconds(delay);
        health = maxHealth;
        isDead = false;

    }

    IEnumerator Slash(float damage, float delay)
    {
        slashed = true;
        SlashCollider.enabled = true;
        SlashRenderer.enabled = false ;

        if (SlashCounter == 0)
        {

            anim.SetTrigger("Swiping");
            SlashCounter = 1;
        } else if(SlashCounter == 1)
        {
            anim.SetTrigger("SwipingTwo");
            SlashCounter = 0;
        }
        


        yield return new WaitForSeconds(delay);

        if (boss != null)
        {
            DamageDone(damage);
            SlashCollider.enabled = false;
        }

        SlashCollider.enabled = false;
        SlashRenderer.enabled = false;
        slashed = false;
        boss = null;
        
    }



    public void DamageDone(float damage)
    {
        boss.Health -= damage;
        boss.Pain.Play();
        
    }


    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.GetComponent<PlayerController>() != null)
        {
            PlayerController tempPlayer = collision.gameObject.GetComponent<PlayerController>();

            if (tempPlayer.isDead)
            {
                nearDeadPlayer = true;
                deadPlayer = tempPlayer;


            }
        }

    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.GetComponent<PlayerController>() != null)
        {
            PlayerController tempPlayer = collision.gameObject.GetComponent<PlayerController>();

            if (tempPlayer.isDead)
            {
                nearDeadPlayer = false;
                deadPlayer = null;


            }
        }

    }

     public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

        if (stream.IsWriting)
        {
           
            stream.SendNext(isDead);
            stream.SendNext(slashed);
            if (boss != null)
            {
                stream.SendNext(boss.Health);
            }

        }
        else
        {
          
            isDead = (bool)stream.ReceiveNext();
            slashed = (bool)stream.ReceiveNext();
            if (boss != null)
            {
                boss.Health = (float)stream.ReceiveNext();
            }

        }








    }

}
