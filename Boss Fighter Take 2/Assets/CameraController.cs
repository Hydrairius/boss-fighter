﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CameraController : MonoBehaviour {

    public const float Y_ANGLE_MIN = 0.0f;
    public const float Y_ANGLE_MAX = 50.0f;

    public Transform lookAt;
    public Transform camTransform;

    private Camera cam;

    public float distance = 3.0f;
    public float currentX = 0.0f;
    public float currentY = 2.0f;
    public float sensitivityX = 4.0f;
    public float sensitivityY = 1.0f;
    public bool foundPlayer = false;

    public PhotonView photonView;

    public Vector3 offset;

    private void Start()
    {
        camTransform = transform;
        cam = Camera.main;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

       
    }

    private void Update()
    {
        GameObject player = GameObject.FindWithTag("Player");

        PlayerController p = player.GetComponent<PlayerController>();
        if (!foundPlayer) { 
        lookAt = player.transform;
        
    }

        if(lookAt != null)
        {
            
            foundPlayer = true;
            photonView = player.GetComponent<PhotonView>();
        }



        if (photonView.IsMine)
        {
            if (p.isDead == false)
            {
                currentX += Input.GetAxis("Mouse X");
                currentY += Input.GetAxis("Mouse Y");

                currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
            }
        }
    }

    private void LateUpdate()
    {
        if (photonView.IsMine)
        {
            Vector3 dir = new Vector3(0, 0, -distance);
            Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
            camTransform.position = (lookAt.position + rotation * dir);
            camTransform.LookAt(lookAt.position);

            lookAt.rotation = Quaternion.Euler(0, currentX, 0);
        }
    }


}
